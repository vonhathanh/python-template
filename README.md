To use post-commit hooks with pre-commit, run:

```pre-commit install --hook-type post-commit```
