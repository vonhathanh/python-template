import pytest

from src.wallet import InsufficientAmount, Wallet


@pytest.fixture
def empty_wallet():
    """
    :return: empty wallet with balance = 0
    """
    return Wallet()


@pytest.fixture
def wallet():
    """
    :return: wallet with 20 coin in it
    """
    return Wallet(20)


@pytest.mark.parametrize("earned, spent, expected", [(30, 20, 30), (10, 10, 20)])
def test_transactions(wallet, earned, spent, expected):
    wallet.add_cash(earned)
    wallet.spend_cash(spent)
    assert wallet.balance == expected


def test_default_initial_amount(empty_wallet):
    assert empty_wallet.balance == 0


def test_setting_initial_amount(wallet):
    assert wallet.balance == 20


def test_wallet_add_cash(wallet):
    wallet.add_cash(90)
    assert wallet.balance == 110


def test_wallet_spend_cash(wallet):
    wallet.spend_cash(10)
    assert wallet.balance == 10


def test_wallet_spend_cash_raises_exception_on_insufficient_amount(wallet):
    with pytest.raises(InsufficientAmount):
        wallet.spend_cash(100)
