import pytest


def capital_str(s: str) -> str:
    return s.capitalize()


def test_capitalize():
    assert capital_str("zzz") == "Zzz"


def test_capitalize_with_non_string_arg():
    with pytest.raises(AttributeError):
        capital_str(9)
